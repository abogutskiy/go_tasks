package linear

import (
	"testing"
)

func TestVectorBaseMethods(t *testing.T) {
	var empty Vector
	if empty.IsValid() {
		t.Errorf("valid 0-d vector")
	}

	v1 := Vector{1, 2, 3}
	if !v1.IsValid() {
		t.Errorf("invalid 3-d vector")
	}

	var v2 Vector
	err := v1.Copy(v2)
	if err == nil {
		t.Errorf("failed to copy vector: expected DemensionError on 0-d vector " +
			"copy, received no error")
	}

	v3 := Vector{1, 2, 3}

	if !v3.Compare(v1) || !v1.Compare(v3) {
		t.Errorf("failed to compare vectors: expected true received false for vectors %v %v", v1, v3)
	}

	if v3.Compare(v2) || v2.Compare(v3) {
		t.Errorf("failed to compare vectors: expected false received true for vectors %v %v", v2, v3)
	}

	vtmp := Vector{1, 2}
	if v3.Compare(vtmp) || vtmp.Compare(v3) {
		t.Errorf("failed to compare vectors: expected false received true for vectors %v %v", vtmp, v3)
	}

	var v4 Vector
	v4.Copy(v3)
	if !v3.Compare(v3) {
		t.Errorf("failed to copy vector: expected %v received %v", v3, v4)
	}

	v5 := Vector{2, 4, 6}
	v6 := CopyVector(v5)
	if !v5.Compare(v5) {
		t.Errorf("failed to copy vector: expected %v received %v", v5, v6)
	}

	v5.Swap(&v3)
	if !v5.Compare(v4) {
		t.Errorf("failed to swap vector: left value: expected %v received %v", v4, v5)
	}
	if !v3.Compare(v6) {
		t.Errorf("failed to swap vector: right value: expected %v received %v", v6, v3)
	}

}

func TestVectorMath(t *testing.T) {
	v1 := Vector{1, 3, -5}
	v2 := Vector{4, -2, -1}
	v3 := Vector{1, 2}
	v4 := Vector{}

	canonical := 3.
	dotProduct, err := DotProduct(v1, v2)
	if err != nil {
		t.Errorf("failed calculate dot product: expected %v received error %v", canonical, err)
	}
	if dotProduct != canonical {
		t.Errorf("failed calculate dot product: expected %v received %v", canonical, dotProduct)
	}

	dotProduct, err = DotProduct(v1, v3)
	if err == nil {
		t.Errorf("dot product calculation error: expected DimensionError received %v", dotProduct)
	}

	dotProduct, err = DotProduct(v1, v4)
	if err == nil {
		t.Errorf("dot product calculation error: expected DimensionError received %v", dotProduct)
	}

	v5 := Vector{1, 2, 3}
	v6 := Vector{1, 2, 3}
	canonical2 := Vector{0, 0, 0}
	err = v6.Sub(v5)
	if err != nil {
		t.Errorf("substruction error: expected %v received error %v", canonical2, err)
	}
	if !v6.Compare(canonical2) {
		t.Errorf("Failed to subsctruct vector from vector: expected %v received %v", canonical2, v6)
	}

	err = v5.Add(v5)
	canonical3 := Vector{2, 4, 6}
	if err != nil {
		t.Errorf("addition error: expected %v received error %v", canonical3, err)
	}
	if !v6.Compare(canonical2) {
		t.Errorf("Failed to add vector to vector: expected %v received %v", canonical3, v5)
	}

	err = v5.Add(v4)
	if err == nil {
		t.Errorf("addition error: expected DimensionError received %v", v5)
	}

	err = v5.Add(v4)
	if err == nil {
		t.Errorf("substruction error: expected DimensionError received %v", v5)
	}
}
