package linear

import (
	"testing"
)

func TestMartrixBaseMethods(t *testing.T) {
	var empty Matrix
	if empty.IsValid() {
		t.Errorf("IsValid method returns true for invalid matrix %v", empty)
	}
	invalid1 := Matrix{{1, 2}, {1}}
	if invalid1.IsValid() {
		t.Errorf("IsValid method returns true for invalid matrix %v", invalid1)
	}
	invalid2 := Matrix{{}, {}}
	if invalid2.IsValid() {
		t.Errorf("IsValid method returns true for invalid matrix %v", invalid2)
	}
	m0 := Matrix{{1}}
	m0_ := Matrix{{1}}
	if !m0.IsValid() {
		t.Errorf("IsValid method returns false for valid matrix %v", m0)
	}

	matrixes := []Matrix{empty, invalid1, invalid2, m0}
	for _, m := range matrixes {
		if !m.Compare(m) {
			t.Errorf("failed to compare matrixes %v %v, expected true, received false", m, m)
		}
	}
	if !m0.Compare(m0_) || !m0_.Compare(m0) {
		t.Errorf("failed to compare matrixes %v %v, expected true, received false", m0, m0_)
	}
	m1 := Matrix{{1, 1}}
	m2 := Matrix{{1}, {1}}
	m3 := Matrix{{1, 2}, {1, 2}}
	matrixes = []Matrix{m1, m2, m3}
	for i := 0; i < len(matrixes); i++ {
		for j := 0; j < len(matrixes); j++ {
			if i != j && matrixes[i].Compare(matrixes[j]) {
				t.Errorf("failed to compare matrixes %v %v, expected false, received true", matrixes[i], matrixes[j])
			}
		}
		if !matrixes[i].Compare(matrixes[i]) {
			t.Errorf("failed to compare matrixes %v %v, expected true, received false", matrixes[i], matrixes[i])
		}
	}

	m2.Copy(m1)
	if !m2.Compare(m1) {
		t.Errorf("failed to copy matrix expected %v received %v", m1, m2)
	}
	var m4 Matrix
	m4.Copy(m1)
	if !m2.Compare(m1) {
		t.Errorf("failed to copy matrix expected %v received %v", m1, m4)
	}

	m5 := Matrix{{1, 2, 3}, {3, 4, 5}}
	m6 := CopyMatrix(m5)
	m5.Swap(&m2)
	if !m5.Compare(m1) || !m2.Compare(m6) {
		t.Errorf("failed to swap matrixes left expected %v left received %v "+
			"right expected %v right received %v", m1, m5, m6, m2)
	}
}

func TestMatrixMath(t *testing.T) {
	empty := Matrix{}
	m0 := Matrix{{1}}
	m1 := Matrix{{1}}

	err := m1.Add(empty)
	if err != DimensionError || !m1.Compare(m0) {
		t.Errorf("failed to add matrix to matrix expected DimensionError, expected value %v received %v error %v", m0, m1, err)
	}

	m1 = CopyMatrix(m0)
	m2 := Matrix{{2, 3}, {4, 5}}
	err = m0.Add(m2)
	if err != DimensionError || !m1.Compare(m0) {
		t.Errorf("failed to add matrix to matrix expected DimensionError, expected value %v received %v error %v", m1, m0, err)
	}

	m1 = CopyMatrix(m0)
	err = m1.Sub(empty)
	if err != DimensionError || !m1.Compare(m0) {
		t.Errorf("failed to substruct matrix from matrix expected DimensionError, expected value %v received %v error %v", m0, m1, err)
	}

	m1 = CopyMatrix(m0)
	err = m0.Sub(m2)
	if err != DimensionError || !m1.Compare(m0) {
		t.Errorf("failed to substruct matrix from matrix expected DimensionError, expected value %v received %v error %v", m1, m0, err)
	}

	err = m1.Add(m0)
	canonical1 := Matrix{{2}}
	if err != nil || !m1.Compare(canonical1) {
		t.Errorf("failed to add matrix from matrix expected %v received %v error %v", canonical1, m1, err)
	}

	m1 = CopyMatrix(m2)
	m3 := Matrix{{1, 2}, {3, 4}}
	err = m1.Sub(m3)
	canonical2 := Matrix{{1, 1}, {1, 1}}
	if err != nil || !m1.Compare(canonical2) {
		t.Errorf("failed to substruct matrix from matrix expected %v received %v error %v", canonical2, m1, err)
	}

	m1 = CopyMatrix(m2)
	err = m1.Add(m3)
	canonical3 := Matrix{{3, 5}, {7, 9}}
	if err != nil || !m1.Compare(canonical3) {
		t.Errorf("failed to substruct matrix from matrix expected %v received %v error %v", canonical3, m1, err)
	}

	m4 := Matrix{
		{1, 0, 1},
		{2, 1, 1},
		{0, 1, 1},
		{1, 1, 2}}
	m5 := Matrix{
		{1, 2, 1},
		{2, 3, 1},
		{4, 2, 2}}
	canonical4 := Matrix{
		{5, 4, 3},
		{8, 9, 5},
		{6, 5, 3},
		{11, 9, 6}}
	err = m4.Multiply(m5)
	if err != nil || !m4.Compare(canonical4) {
		t.Errorf("failed to multiply two matrixes expected %v received %v error %v", canonical4, m4, err)
	}

	m6 := Matrix{{1, 2, 3, 4}, {1, 2, 3, 4}}
	m7 := CopyMatrix(m6)
	m8 := Matrix{{1, 2}, {1, 2}, {1, 2}}

	err = m6.Multiply(m8)
	if err != DimensionError || !m6.Compare(m7) {
		t.Errorf("failed to multiply two matrixes expected DimensionError expected value %v received %v error %v", m7, m6, err)
	}

	m6 = CopyMatrix(m7)
	m6.Multiply(empty)
	if err != DimensionError || !m6.Compare(m7) {
		t.Errorf("failed to multiply two matrixes expected DimensionError expected value %v received %v error %v", m7, m6, err)
	}

	m6 = CopyMatrix(m7)
	v := Vector{1, 1, 1, 1}
	canonical5 := Matrix{{10}, {10}}
	err = m6.MultiplyByVector(v)
	if err != nil || !m6.Compare(canonical5) {
		t.Errorf("failed to multiply matrixe by Vector expected expected %v received %v error %v", canonical5, m6, err)
	}

}
