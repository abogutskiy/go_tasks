package linear

type Vector []float64

func CopyVector(v Vector) Vector {
	return append(Vector{}, v...)
}

func (v *Vector) Init(size int) error {
	if size > 0 {
		*v = make(Vector, size)
		return nil
	}
	return DimensionError

}

func (v *Vector) Copy(other Vector) error {
	if other.IsValid() {
		(*v) = append(Vector{}, other...)
		return nil
	}
	return DimensionError
}

func (v *Vector) Swap(other *Vector) {
	tmp := *v
	*v = *other
	*other = tmp
}

func (v Vector) IsValid() bool {
	return len(v) > 0
}

func (v Vector) Compare(other Vector) bool {
	if len(v) != len(other) {
		return false
	}
	for i, _ := range v {
		if v[i] != other[i] {
			return false
		}
	}
	return true
}

/*
* Math functions
 */

func DotProduct(a, b Vector) (float64, error) {
	if len(a) != len(b) || len(a) == 0 {
		return 0, DimensionError
	}
	var product float64 = 0
	for i, _ := range a {
		product += a[i] * b[i]
	}
	return product, nil
}

func (v Vector) Add(other Vector) error {
	if len(v) != len(other) || len(v) == 0 {
		return DimensionError
	}
	for i, _ := range v {
		v[i] += other[i]
	}
	return nil
}

func (v Vector) Sub(other Vector) error {
	if len(v) != len(other) || len(v) == 0 {
		return DimensionError
	}
	for i, _ := range v {
		v[i] -= other[i]
	}
	return nil
}
