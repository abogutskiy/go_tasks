package linear

type Matrix [][]float64

func CopyMatrix(m Matrix) Matrix {
	nm := make([][]float64, len(m))
	for i, _ := range m {
		nm[i] = append([]float64{}, m[i]...)
	}
	return nm
}

func (m *Matrix) Init(ysize, xsize int) error {
	if ysize > 0 && xsize > 0 {
		*m = make([][]float64, ysize)
		for i := 0; i < ysize; i++ {
			(*m)[i] = make([]float64, xsize)
		}
		return nil
	}
	return DimensionError
}

func (m *Matrix) Copy(other Matrix) error {
	if other.IsValid() {
		(*m) = make([][]float64, len(other))
		for i, row := range other {
			(*m)[i] = append([]float64{}, row...)
		}
		return nil
	}
	return DimensionError
}

func (m *Matrix) Swap(other *Matrix) {
	tmp := *m
	*m = *other
	*other = tmp
}

func (m Matrix) IsValid() bool {
	if len(m) < 1 {
		return false
	}
	dx := len(m[0])
	if dx == 0 {
		return false
	}
	for i := 0; i < len(m); i++ {
		if len(m[i]) != dx {
			return false
		}
	}
	return true
}

func (m Matrix) Compare(other Matrix) bool {
	if len(m) != len(other) {
		return false
	}
	if len(m) == 0 {
		return true
	}
	for i, row := range m {
		if len(row) != len(other[i]) {
			return false
		}
		for j := 0; j < len(row); j++ {
			if m[i][j] != other[i][j] {
				return false
			}
		}
	}
	return true
}

/*
* Math functions
 */

func (m Matrix) Add(other Matrix) error {
	if !m.IsValid() || !other.IsValid() || len(m) != len(other) || len(m[0]) != len(other[0]) {
		return DimensionError
	}

	for i := 0; i < len(m); i++ {
		for j := 0; j < len(m[i]); j++ {
			m[i][j] += other[i][j]
		}
	}
	return nil
}

func (m Matrix) Sub(other Matrix) error {
	if !m.IsValid() || !other.IsValid() || len(m) != len(other) || len(m[0]) != len(other[0]) {
		return DimensionError
	}

	for i := 0; i < len(m); i++ {
		for j := 0; j < len(m[i]); j++ {
			m[i][j] -= other[i][j]
		}
	}
	return nil
}

func (m *Matrix) Multiply(other Matrix) error {
	if !(*m).IsValid() || !other.IsValid() || len((*m)[0]) != len(other) {
		return DimensionError
	}
	n := len(other) // == len((*m)[0])
	var mnew Matrix
	mnew.Init(len(*m), len(other[0]))
	for i := 0; i < len(mnew); i++ {
		for j := 0; j < len(mnew[0]); j++ {
			for k := 0; k < n; k++ {
				mnew[i][j] += (*m)[i][k] * other[k][j]
			}
		}
	}
	*m = mnew
	return nil
}

func (m *Matrix) MultiplyByVector(v Vector) error {
	var other Matrix
	other.Init(len(v), 1)
	for i, v := range v {
		other[i][0] = v
	}
	return m.Multiply(other)
}
