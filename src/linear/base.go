package linear

import (
	"errors"
)

var DimensionError = errors.New("dimension error")
